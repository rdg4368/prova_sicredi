*** Settings ***
Documentation     Suite de teste Cadastro com sucesso

Resource        ${EXECDIR}/resources/Base.robot
Resource        ${EXECDIR}/resources/page/CadastroPage.robot

Suite Setup         Abrir Navegador
Test Teardown       Fechar Navegador

*** Test Cases ***
Cenario 1: Cadastrar Usuário
    Dado que eu acesse o sistema XPTO para alterar o layout
    E cadastrar um usuário
    Quando submeter o Cadastro
    Então o sistema me retorna mensagem de sucesso
 

