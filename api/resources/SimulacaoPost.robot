*** Settings ***
Library    Collections
Library    RequestsLibrary
Library    BuiltIn

Resource         ${EXECDIR}/resources/Base.robot


*** Variable ***
${SIMULACOES}           simulacoes



*** Keywords ***
Cadastrar uma Simulação
    ${HEADERS}          Create Dictionary           content-type=application/json
    ${RESPOSTA}         POST On Session             API                         ${SIMULACOES}
    ...                 data={"nome": "Nova Simulação Automatizado", "cpf": 97093236014, "email": "oxi@email.com","valor": 1200, "parcelas": 3,"seguro": true}
    ...                 headers=${HEADERS}
    Log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}


Cadastrar Simulação com CPF já Cadastrado
    ${HEADERS}          Create Dictionary           content-type=application/json
    ${RESPOSTA}         POST On Session             API                         ${SIMULACOES}
    ...                 data={"nome": "CPF ja cadastrado", "cpf": 66414919004, "email": "ue@email.com","valor": 1200, "parcelas": 3,"seguro": true}
    ...                 headers=${HEADERS}
    Log                 ${RESPOSTA.text}
    Set Test Variable   ${RESPOSTA}