import openpyxl
from openpyxl import load_workbook

#METODO LEITURA PLANILHA
def ler_dado_celula(aba: str, celula: str):
    planilha = load_workbook('data/MassaDados.xlsx')
    sheet_ranges = planilha[aba]
    return sheet_ranges[celula].value