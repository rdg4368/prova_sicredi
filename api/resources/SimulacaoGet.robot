***Settings***
Library                     Collections
Library                     RequestsLibrary
Library                     BuiltIn

Resource                    ${EXECDIR}/resources/Base.robot
Resource                    ${EXECDIR}/resources/SimulacaoGet.robot

***Variables***
${SIMULACOES}               simulacoes


***Keywords***
#Realiza a requisição das simulações
Requisitar Simulação "${CPF}"  
    ${RESPOSTA}                  GET On Session    API    ${SIMULACOES}/${CPF}
    Log                         ${RESPOSTA.text}
    Set Test Variable           ${RESPOSTA}


#Realiza a requisição das simulações
Requisitar Simulação todas 
    ${RESPOSTA}                  GET On Session    API    ${SIMULACOES}
    Log                         ${RESPOSTA.text}
    Set Test Variable           ${RESPOSTA}

