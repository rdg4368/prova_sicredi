***Settings***
Library             SeleniumLibrary
Library             String
Library             DateTime
Library             ${EXECDIR}/utils/LerPlanilha.py

Resource            ${EXECDIR}/resources/Base.robot


***Variables***
${data_Name}                      Teste Sicredi 

***Keywords***
Dado que eu queira excluir um usuário do sistema
  #CAMPO PESQUISAR POR NOME
  Wait Until Element Is Visible       name=customerName                 
  Input Text                          name=customerName               ${data_Name}

  #BTN ATUALIZAR TABELA
  Wait Until Element Is Visible       css=div.floatR.l5 > a
  Click Element                       css=div.floatR.l5 > a

Quando excluir
  #CHECK SELECIONAR ITENS
  Wait Until Element Is Visible       css=div.scroll-if-required table.table.table-bordered.grocery-crud-table.table-hover thead tr.filter-row.gc-search-row td.no-border-right div.floatL.t5 input.select-all-none
  Click Element                       css=div.scroll-if-required table.table.table-bordered.grocery-crud-table.table-hover thead tr.filter-row.gc-search-row td.no-border-right div.floatL.t5 input.select-all-none

  #BTN EXCLUIR
  Click Element                       css=tr.filter-row.gc-search-row > td.no-border-left > div.floatL > a

E confirmar a exclusão
  #POPUP CONFIRMAR EXCLUIR
  Wait Until Element Is Visible       css=p.alert-delete-multiple-one
  ${MensagemDelete}=                  Get Text                              css=p.alert-delete-multiple-one
  Should Be Equal                     ${MensagemDelete}                     Are you sure that you want to delete this 1 item?
  Sleep                               1
  Capturar tela                       01 - DeletarRegistro.png 
  #Confirmar o Delete
  Click Element                       css=button.btn.btn-danger.delete-multiple-confirmation-button


Então o sistema me retorna mensagem de sucesso
  #VALIDAR MENSAGEM DE EXCLUSÃO
  Wait Until Element Is Visible       xpath=(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::p[1]
  ${MensagemConfirmeDelete}=          Get Text                              xpath=(.//*[normalize-space(text()) and normalize-space(.)='Close'])[1]/following::p[1]
  Should Be Equal                     ${MensagemConfirmeDelete}             Your data has been successfully deleted from the database.
  Sleep                               1
  Capturar tela                       02 - ConfirmarDeletarRegistro.png 
  #AGUARDAR PARA SALVAR O PRINT E RENOMEAR O ARQUIVO
  Sleep                               1