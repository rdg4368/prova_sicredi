***Settings***
Library             SeleniumLibrary
Library             String
Library             DateTime
Library             ${EXECDIR}/utils/LerPlanilha.py

Resource            ${EXECDIR}/resources/Base.robot


***Variables***

${btn-Record}  						 link=Add Record
${field-customerName}    		    id=field-customerName    		
${field-contactLastName}    	    id=field-contactLastName    	
${field-contactFirstName}    	    id=field-contactFirstName    	
${field-phone}    			       id=field-phone    			   
${field-addressLine1}    		    id=field-addressLine1    		
${field-addressLine2}    		    id=field-addressLine2    		
${field-city}    			          id=field-city    			   
${field-state}    		      	 id=field-state    		      	
${field-postalCode}    		       id=field-postalCode    		   
${field-country}    			       id=field-country    			
${field-salesRepEmployeeNumber}   id=field-salesRepEmployeeNumber
${field-creditLimit}    		    id=field-creditLimit    		
${btn-save}    	                id=form-button-save      

${Mensagem1}                      Your data has been successfully stored into the database. Edit Record or Go back to list
${Mensagem2}                      Your data has been successfully stored into the database. Edit Record or
${Mensagem3}                      Your data has been successfully stored into the database. or   

***Keywords***
Dado que eu acesse o sistema XPTO para alterar o layout
   Wait Until Element Is Visible       xpath=//select[@id='switch-version-select']
   Select From List By Value           xpath=//select[@id='switch-version-select']       /v1.x/demo/my_boss_is_in_a_hurry/bootstrap-v4
   Sleep                               1
   Capturar tela                       01 - Alteração do Layout.png

E cadastrar um usuário
	${data_customerName}   				  Ler dado da planilha          CADASTRAR_USUARIO   		    B2
	${data_LastName}   					  Ler dado da planilha          CADASTRAR_USUARIO   		    C2
	${data_FirstName}    				  Ler dado da planilha          CADASTRAR_USUARIO    	       D2
	${data_phone}    						  Ler dado da planilha          CADASTRAR_USUARIO    		    E2
	${data_addressLine1}  				  Ler dado da planilha          CADASTRAR_USUARIO            F2
	${data_addressLine2}  				  Ler dado da planilha          CADASTRAR_USUARIO            G2
	${data_city}    						  Ler dado da planilha          CADASTRAR_USUARIO    		    H2
	${data_state}    						  Ler dado da planilha          CADASTRAR_USUARIO    		    I2
	${data_postalCode}    				  Ler dado da planilha          CADASTRAR_USUARIO    	       J2
	${data_country }    					  Ler dado da planilha          CADASTRAR_USUARIO    	       K2
	${data_EmployeeNumber}			     Ler dado da planilha          CADASTRAR_USUARIO            L2
	${data_creditLimit}   				  Ler dado da planilha          CADASTRAR_USUARIO    	       M2 

   click link  				${btn-Record}  	   
   Input Text    			${field-customerName}    						       ${data_customerName}  	
   Input Text    			${field-contactLastName}    					       ${data_LastName}   	
   Input Text    			${field-contactFirstName}    					       ${data_FirstName}    	
   Input Text    			${field-phone}    			    				       ${data_phone}    		
   Input Text    			${field-addressLine1}    						       ${data_addressLine1}   
   Input Text    			${field-addressLine2}    						       ${data_addressLine2}   
   Input Text    			${field-city}    			    				          ${data_city}    		
   Input Text    			${field-state}    		      					    ${data_state}    		
   Input Text    			${field-postalCode}    		    				       ${data_postalCode}    	
   Input Text    			${field-country}    							          ${data_country }    	
   Input Text    			${field-salesRepEmployeeNumber} 				       ${data_EmployeeNumber} 
   Input Text    			${field-creditLimit}    						       ${data_creditLimit}    
   Capturar tela         02 - Cadastro.png

Quando submeter o Cadastro
   Click Element  			${btn-save}   

Então o sistema me retorna mensagem de sucesso
   Wait Until Element Is Visible               id=report-success
   ${MensagemPage}=      Get Text              id=report-success
   Run Keyword If        '${MensagemPage}' == '${Mensagem1}'   Should Be Equal     ${MensagemPage}     ${Mensagem1} 
   Run Keyword If        '${MensagemPage}' == '${Mensagem2}'   Should Be Equal     ${MensagemPage}     ${Mensagem2} 
   Run Keyword If        '${MensagemPage}' == '${Mensagem3}'   Should Be Equal     ${MensagemPage}     ${Mensagem3}
   Sleep                               1
   Capturar tela                       03 - Validaçao de Mensagem.png