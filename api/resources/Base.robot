***Settings***
Documentation               Documentação da API: http://localhost:8080/swagger-ui.html#/Simulações
Library                     Collections
Library                     RequestsLibrary
Library                     BuiltIn

***Variables***
${URL}                      http://localhost:8080/api/v1/


***Keywords***

Conectar api
    Create Session                          API                         ${URL}

Validar Status Code
    [Arguments]                             ${STATUSCODE_DESEJADO}
    Log                                     ${STATUSCODE_DESEJADO}
    Should Be Equal As Strings              ${RESPOSTA.status_code}     ${STATUSCODE_DESEJADO}

Validar o Reason
    [Arguments]                             ${REASON_DESEJADO}
    Log                                     ${RESPOSTA.reason}
    Should Be Equal As Strings              ${RESPOSTA.reason}          ${REASON_DESEJADO}

Validar Mensagem
    Dictionary Should Contain Key           ${RESPOSTA.json()[mensagem]}


