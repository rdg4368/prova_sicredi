*** Settings ***
Resource         ${EXECDIR}/resources/Base.robot
Resource         ${EXECDIR}/resources/SimulacaoGet.robot
Resource         ${EXECDIR}/resources/SimulacaoPost.robot
Resource         ${EXECDIR}/resources/SimulacaoPut.robot
Resource         ${EXECDIR}/resources/SimulacaoDelete.robot

Suite Setup      Conectar API

*** Test Cases ***
Consultar Simulação Retorna Todas
    Requisitar Simulação todas 
    Validar Status Code             200
    #Validar Mensagem                    
    #Validar o Reason                OK

Consultar Simulação Retorna Especifica
    Requisitar Simulação "${17822386034}"  
    Validar Status Code             200
    #Validar Mensagem                    
    #Validar o Reason                OK

Cadastrar uma Simulação
    Cadastrar uma Simulação
    Validar Status Code             201    
    #Validar o Reason                 

Atualizar uma Simulacão
    Alterar Simulação "${17822386034}"
    Validar Status Code             200 

Excluir uma Simulação
    Deletar Simulação "${97093236014}"
    Validar Status Code             200 

Cadastrar Simulação com CPF já Cadastrado
    Cadastrar Simulação com CPF já Cadastrado
    Validar Status Code             400    