*** Settings ***
Documentation       Principal
Library             SeleniumLibrary
Library             String
Library             DateTime
Library             ${EXECDIR}/utils/LerPlanilha.py
Resource            ${EXECDIR}/resources/Base.robot

*** Variables ***

*** Keywords ***
Abrir Navegador
  ${URL}=                             Ler dado da planilha             url           b2
  ${NAVEGADOR}=                       Ler dado da planilha             url           c2
  Open Browser                        ${URL}                      ${NAVEGADOR}          #options=add_argument("--disable-web-security");add_argument("--ignore-certificate-errors");add_argument("--allow-insecure-localhost");add_argument("--allow-cross-origin-auth-prompt")
  Maximize Browser Window
  Set Selenium Implicit Wait          10

Ler dado da planilha
  [Arguments]       ${Sheetname}        ${cell}
  ${cell_data}=     ler_dado_celula     ${Sheetname}      ${cell}
  [Return]          ${cell_data}

Pegar o nome do test-case
    ${NOME_CASO_TESTE}=     Get Variable Value      ${TEST NAME}
    ${NOME_CASO_TESTE}=     Replace String          ${NOME_CASO_TESTE}  ${SPACE}    _
    ${NOME_CASO_TESTE}=     Replace String          ${NOME_CASO_TESTE}  (   _
    ${NOME_CASO_TESTE}=     Replace String          ${NOME_CASO_TESTE}  )   _
    ${NOME_CASO_TESTE}=     Replace String          ${NOME_CASO_TESTE}  \"  _
    ${NOME_CASO_TESTE}=     Replace String          ${NOME_CASO_TESTE}  :   _
    ${NOME_CASO_TESTE}=     Replace String          ${NOME_CASO_TESTE}  \'  _ 
    [Return]                ${NOME_CASO_TESTE}    
    
Capturar tela                    
    [Arguments]                  ${NOME_EVIDENCIA}
    ${NOME_CASO_TESTE}=          Pegar o nome do test-case 
    ${DATA_ATUAL}=               Get Current Date                     result_format= %d.%m.%Y.  
    Set Screenshot Directory     evidencias\\${DATA_ATUAL}\\${NOME_CASO_TESTE}
    Capture Page Screenshot      ${NOME_EVIDENCIA}

Fechar Navegador
  Close Browser
