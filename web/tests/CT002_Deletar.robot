*** Settings ***
Documentation     Suite de teste Cadastro com sucesso

Resource        ${EXECDIR}/resources/Base.robot
Resource        ${EXECDIR}/resources/page/DeletePage.robot

Suite Setup         Abrir Navegador
Test Teardown       Fechar Navegador

*** Test Cases ***
Cenario 1: Deletar Usuário
    Dado que eu queira excluir um usuário do sistema
    Quando excluir
    E confirmar a exclusão
    Então o sistema me retorna mensagem de sucesso

