*** Settings ***
Library    Collections
Library    RequestsLibrary
Library    BuiltIn

Resource         ${EXECDIR}/resources/Base.robot


*** Variable ***
${SIMULACOES}                  simulacoes


*** Keywords ***
Deletar Simulação "${CPF}"
    ${HEADERS}                  Create Dictionary           content-type=application/json
    ${RESPOSTA}                 Delete On Session    API    ${SIMULACOES}/${CPF}
    ...                         data={"nome": "Mudei", "cpf": 172678402134, "email": "Carlosl@email.com","valor": 1200, "parcelas": 100,"seguro": true}
    Log                         ${RESPOSTA.text}
    Set Test Variable           ${RESPOSTA}