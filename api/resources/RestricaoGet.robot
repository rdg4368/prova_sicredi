***Settings***
Library                     Collections
Library                     RequestsLibrary
Library                     BuiltIn

Resource                    ${EXECDIR}/resources/Base.robot
***Variables***
${RESTRICOES}               restricoes


***Keywords***
#Realiza a requisição das simulações
Requisitar Restricao "${CPF}"  
    ${RESPOSTA}                  GET On Session    API    ${RESTRICOES}/${CPF}
    Log                         ${RESPOSTA.text}
    Set Test Variable           ${RESPOSTA}